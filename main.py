from decimal import Decimal, DecimalException


# 1
# Напишите программу, которая реализует функциональность считывания с клавиатуры стоимости товара.
# При этом стоит учесть, что пользователь может ввести не цифры,
# а смесь цифр и букв или отрицательное число. При необходимости создайте пользовательское исключение
# и обработайте его (например, для отрицательных чисел).
class UserException(BaseException):
    def __init__(self, message):
        super().__init__()
        self.message = message

    def get_exception_message(self): return self.message


negative_exception = UserException('You entered negative number')
price = input('Please, enter the price: ')
try:
    price = Decimal(price)
    try:
        if price < 0:
            raise UserException('You entered negative number')
    except UserException as err:
        print(err.get_exception_message())
    else:
        print(price)
except DecimalException:
    print('you entered incorrect value')


# 2
# Модифицируйте класс Группа (задание прошлой лекции) так,
# чтобы при попытке добавления в группу более 10-ти студентов,
# было возбужденно пользовательское исключение. Таким образом нужно создать еще
# и пользовательское исключение для этой ситуации. И обработать его.
class UserException(BaseException):
    def __init__(self, message):
        super().__init__()
        self.message = message

    def get_exception_message(self): return self.message


class Human:
    def __init__(self, name, surname, age):
        self.name = name
        self.surname = surname
        self.age = age

    def print_info(self):
        print(f'I am human, my name: {self.name}, surname: {self.surname}, age: {self.age}')


class Student(Human):
    def __init__(self, name, surname, age):
        super().__init__(name, surname, age)

    def print_info(self):
        print(f'I am student, my name: {self.name}, surname: {self.surname}, age: {self.age}')

    def __str__(self):
        return f'name: {self.name}, surname: {self.surname}, age: {self.age}'


class Group:
    def __init__(self, student_list=None):
        if student_list is None:
            student_list = []
        try:
            if len(student_list) >= 11:
                raise UserException('Only 10 students are allowed in one group')
        except UserException as err:
            print(err.get_exception_message())
        self.student_list = student_list
        self.student_dict = {(student.surname, student.name): student for student in student_list}

    def add_student(self, student: Student):
        self.student_list.append(student)
        self.student_dict[(student.surname, student.name)] = student

    def delete_student(self, student: Student):
        self.student_list.remove(student)
        del self.student_dict[(student.surname, student.name)]

    def find_by_surname(self, surname):
        return self.student_dict.get(surname)

    def __str__(self):
        return str(list(map(str, self.student_list)))


student_list = [Student('Leva', 'Rossum', 18) for student in range(11)]
student_1 = Student('Leva', 'Rossum', 18)
group_1 = Group([student_1])
group_2 = Group(student_list)


# 1
# Создайте класс «Прямоугольник», у которого присутствуют два поля (ширина и высота).
# Реализуйте метод сравнения прямоугольников по площади. Также реализуйте методы сложения
# прямоугольников (площадь суммарного прямоугольника должна быть равна сумме площадей
# прямоугольников, которые вы складываете). Реализуйте методы умножения прямоугольника
# на число n (это должно увеличить площадь базового прямоугольника в n раз).

class Rectangle:
    def __init__(self, width, height):
        self.width = width
        self.height = height

    @staticmethod
    def square(self):
        return self.width * self.height

    def __add__(self, other):
        if isinstance(other, Rectangle):
            return self.square(self) + self.square(other)
        return NotImplemented

    def __mul__(self, other):
        if isinstance(other, (int, float)):
            return self.square(self) * other
        return NotImplemented

    def __eq__(self, other):
        if isinstance(other, Rectangle):
            return self.square(self) == self.square(other)
        return NotImplemented

    def __gt__(self, other):
        if isinstance(other, Rectangle):
            return self.square(self) > self.square(other)
        return NotImplemented

    def __lt__(self, other):
        if isinstance(other, Rectangle):
            return not self > other
        return NotImplemented

    def __ge__(self, other):
        if isinstance(other, Rectangle):
            return self > other or self == other
        return NotImplemented

    def __le__(self, other):
        if isinstance(other, Rectangle):
            return not self > other or self == other
        return NotImplemented

    def __str__(self):
        return f"Rectangle: width = {self.width}, height = {self.height}"


# 2
# Создайте класс «Правильная дробь» и реализуйте методы сравнения,
# сложения, вычитания и произведения для экземпляров этого класса.

from math import lcm, gcd


class Fraction:
    def __init__(self, numerator, denominator):
        self.numerator = numerator
        self.denominator = denominator

    @staticmethod
    def proper_fraction(a, b):
        x = gcd(a, b)
        a = str(int(a / x))
        b = str(int(b / x))
        return a, b

    def __add__(self, other):
        if isinstance(other, Fraction):
            denominator = lcm(self.denominator, other.denominator)
            numerator = int(
                denominator / self.denominator * self.numerator + denominator / other.denominator * other.numerator)
            return "/".join(self.proper_fraction(numerator, denominator))
        return NotImplemented

    def __sub__(self, other):
        if isinstance(other, Fraction):
            denominator = lcm(self.denominator, other.denominator)
            numerator = int(
                denominator / self.denominator * self.numerator - denominator / other.denominator * other.numerator)
            return "/".join(self.proper_fraction(numerator, denominator))
        return NotImplemented

    def __mul__(self, other):
        if isinstance(other, Fraction):
            denominator = self.denominator * other.denominator
            numerator = self.numerator * other.numerator
            return "/".join(self.proper_fraction(numerator, denominator))
        return NotImplemented

    def __eq__(self, other):
        if isinstance(other, Fraction):
            return self.numerator / self.denominator == other.numerator / other.denominator
        return NotImplemented

    def __lt__(self, other):
        if isinstance(other, Fraction):
            return self.numerator / self.denominator < other.numerator / other.denominator
        return NotImplemented

    def __gt__(self, other):
        return not self < other

    def __le__(self, other):
        if isinstance(other, Fraction):
            return self < other or self == other
        return NotImplemented

    def __ge__(self, other):
        if isinstance(other, Fraction):
            return self > other or self == other
        return NotImplemented

    def __ne__(self, other):
        if isinstance(other, Fraction):
            return not other == self


# 1
# Реализуйте генераторную функцию, которая будет возвращать по
# одному члену геометрической прогрессии с указанным множителем.
# Генератор должен остановить свою работу или по достижению указанной
# границы, или при передаче команды на завершение.

def geometric_progression(start, stop, q):
    while start <= stop:
        yield start
        start *= q


# 2
# Реализуйте свой аналог генераторной функции range(). Да, вы теперь
# знаете, что это - генератор.


def my_range(start=0, stop=None, step=1):
    if step == 0:
        raise ValueError("range() arg 3 must not be zero")
    if stop is None:
        stop = start
        start = 0
    if step < 0:
        while stop < start:
            yield start
            start += step
    else:
        while start < stop:
            yield start
            start += step


# 3
# Напишите функцию-генератор, которая будет возвращать простые числа.
# Верхняя граница этого диапазона должна быть задана параметром этой
# функции.
def is_prime(n):
    for i in range(2, int(n ** 0.5) + 1):
        if n % i == 0:
            return False
    return True


def prime_numbers(n):
    for i in range(2, n + 1):
        if is_prime(i):
            yield i


# 4
# Напишите выражение-генератор для заполнения списка. Список должен
# быть заполнен кубами чисел от 2 и до указанной вами величины.

cubic_list = [num ** 3 for num in range(2, int(input()))]

# 1) Создайте декоратор, который зарегистрирует декорируемый класс в
# списке классов.
classes = []


def decorate_cls(cls):
    classes.append(cls)

    def inner_func(*args, **kwargs):
        new_instance = cls(*args, **kwargs)
        return new_instance

    return inner_func


@decorate_cls
class Box:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return f"Box [x: {self.x}], y: {self.y}, z: {self.z}"


print(classes)


# 2) Создайте декоратор класса с параметром. Параметром должна быть
# строка, которая должна дописываться (слева) к результату работы метода
# __str__.


def str_decorator(text):
    def func_decor(func):
        def change_str(*args, **kwargs):
            return text + func(*args, **kwargs)

        return change_str

    return func_decor


class Box1:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    @str_decorator("Beautiful ")
    def __str__(self):
        return f"Box [x: {self.x}], y: {self.y}, z: {self.z}"


box = Box1(1, 2, 3)
print(box)


# 3) Для класса Box напишите статический метод, который будет подсчитывать
# суммарный объем двух ящиков, которые будут его параметрами.


class Box3:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    @staticmethod
    def volume_of_boxes(x, y, z):
        return 2 * x * y * z

    def __str__(self):
        return f"Box [x: {self.x}], y: {self.y}, z: {self.z}"


# 1
# Создайте дескриптор, который будет делать поля класса управляемые им
# доступными только для чтения.

class Box4:
    __slots__ = ("x", "y", "z")

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return f"Box [x: {self.x}], y: {self.y}, z: {self.z}"


# 2
# Реализуйте функционал, который будет запрещать установку полей класса
# любыми значениями, кроме целых чисел. Т.е., если тому или иному полю
# попытаться присвоить, например, строку, то должно быть возбужденно
# исключение.


class Box5:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __setattr__(self, key, value):
        if isinstance(value, int):
            self.__dict__[key] = value
        else:
            raise AttributeError("Must be int")

    def __str__(self):
        return f"Box [x: {self.x}], y: {self.y}, z: {self.z}"


box = Box5(1, 2, "s")

# 3
# Реализуйте свойство класса, которое обладает следующим
# функционалом: при установки значения этого свойства в файл с заранее
# определенным названием должно сохранятся время (когда устанавливали
# значение свойства) и установленное значение.

from time import asctime


class Cat:
    file_to_save = 'file.txt'

    def __init__(self, __name, age):
        self.name = __name
        self.age = age

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name = value
        file_to_write = open(Cat.file_to_save, 'w')
        result = str(asctime()) + ' : ' + value
        file_to_write.write(result)
        file_to_write.close()

    def __str__(self):
        return "Cat [name = " + str(self.__name) + ", age = " + str(self.age) + "]"


cat = Cat("John", 2)
cat.name = "Wiskas"


# 1
# Создайте декоратор, который будет подсчитывать, сколько раз была
# вызвана декорируемая функция.

def counter(func):
    counter.count = 0

    def wrap(*args, **kwargs):
        counter.count += 1
        print("Number of times {} has been called so far {}".format(func.__name__, counter.count))
        func(*args, **kwargs)

    return wrap


@counter
def foo():
    return None


foo()
foo()
foo()
foo()

# 2
# Создайте декоратор, который зарегистрирует декорируемую функцию в
# списке функций, для обработки последовательности.
functions = []


def registration_func(func):
    functions.append(func)
    return func


@registration_func
def get_term_of_sequence(sequence, first_term, amount):
    for _ in range(amount):
        yield first_term
        first_term = sequence(first_term)


# 3
# Предположим, в классе определен метод __str__, который возвращает
# строку на основании класса. Создайте такой декоратор для этого метода,
# чтобы полученная строка сохранялась в текстовый файл, имя которого
# совпадает с именем класса, метод которого вы декорировали.
def to_file(file_name):
    def write_str(func):
        def write_func(*args, **kwargs):
            file_to_write = open(file_name, 'w')
            file_to_write.write(func(*args, **kwargs))
            file_to_write.close()
            return func(*args, **kwargs)

        return write_func

    return write_str


class MakeStr:
    def __init__(self, name):
        self.name = name

    @to_file("MakeStr.txt")
    def __str__(self):
        return f"Your name is {self.name}"


d = MakeStr("Elena")
print(d)

# 4
# Создайте декоратор с параметрами для проведения хронометража работы
# той или иной функции. Параметрами должны выступать то, сколько раз нужно
# запустить декорируемую функцию и в какой файл сохранить результаты
# хронометража. Цель - провести хронометраж декорируемой функции.

import time


def check_time(times, file_name):
    def my_decorator(func):
        def check(*args, **kwargs):
            start = time.time()
            for _ in range(times):
                func(*args, **kwargs)
            result = time.time() - start
            print(result)
            file_to_write = open(file_name, 'w')
            file_to_write.write(str(result) + " s")
            file_to_write.close()
            return func(*args, **kwargs)

        return check

    return my_decorator


@check_time(10, "result.txt")
def foo2(n):
    return n * 10 ** 25


print(foo2(5))
